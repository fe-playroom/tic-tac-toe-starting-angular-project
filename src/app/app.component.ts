import {Component} from '@angular/core';
import {PlayerComponent} from "./components/player/player.component";
import {GameBoardComponent} from "./components/game-board/game-board.component";
import {LogComponent} from "./components/log/log.component";
import {GameOverComponent} from "./components/game-over/game-over.component";
import {WINNING_COMBINATIONS} from "./winning-combinations";
import {NgClass} from "@angular/common";

export enum PlayerSymbol {
  X = 'X',
  O = 'O'
}

export interface Player {
  playerName: string,
  playerSymbol: PlayerSymbol
}

export interface GameRound {
  "square": {
    "rowIndex": number,
    "colIndex": number,
  },
  "player": PlayerSymbol
}

const INITIAL_GAME_BOARD: (PlayerSymbol | null)[][] = [
  [null, null, null],
  [null, null, null],
  [null, null, null]
]

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [PlayerComponent, GameBoardComponent, LogComponent, GameOverComponent, NgClass],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  protected readonly FIRST_PLAYER_SYMBOL: PlayerSymbol = PlayerSymbol.X;
  protected readonly SECOND_PLAYER_SYMBOL: PlayerSymbol = PlayerSymbol.O;

  firstPlayerName: string = 'Player 1';
  secondPlayerName: string = 'Player 2';
  gameBoard: (PlayerSymbol | null)[][] = INITIAL_GAME_BOARD;
  activePlayer: PlayerSymbol = PlayerSymbol.X;
  gameRounds: GameRound[] = [];
  winner: string | undefined = undefined;
  hasDraw: boolean = false;

  updatePlayerName(newPlayerName: string, playerSymbol: PlayerSymbol): void {
    switch (playerSymbol) {
      case PlayerSymbol.X: {
        this.firstPlayerName = newPlayerName;
        break;
      }
      case PlayerSymbol.O: {
        this.secondPlayerName = newPlayerName;
        break;
      }
    }
  }

  updateGameBoard(rowIndex: number, colIndex: number): void {
    this.gameBoard[rowIndex][colIndex] = this.activePlayer;
    this.updateGameRounds(rowIndex, colIndex, this.activePlayer)
    this.changeActivePlayer();
  }

  changeActivePlayer(): void {
    if (this.activePlayer === PlayerSymbol.X) {
      this.activePlayer = PlayerSymbol.O;
    } else {
      this.activePlayer = PlayerSymbol.X;
    }
  }

  updateGameRounds(rowIndex: number, colIndex: number, player: PlayerSymbol): void {
    let gameRound: GameRound = {square: {rowIndex, colIndex}, player: player};
    this.gameRounds.push(gameRound);
    this.deriveWinner(
      this.gameBoard,
      [{playerName: this.firstPlayerName, playerSymbol: this.FIRST_PLAYER_SYMBOL},
        {playerName: this.secondPlayerName, playerSymbol: this.SECOND_PLAYER_SYMBOL}]);
    this.deriveDraw();
  }

  deriveWinner(gameBoard: (PlayerSymbol | null)[][], players: Player[]): void {
    let winner: string | undefined = '';
    for (const combination of WINNING_COMBINATIONS) {
      const firstSquareSymbol: PlayerSymbol | null = gameBoard[combination[0].row][combination[0].column];
      const secondSquareSymbol: PlayerSymbol | null = gameBoard[combination[1].row][combination[1].column];
      const thirdSquareSymbol: PlayerSymbol | null = gameBoard[combination[2].row][combination[2].column];

      if (firstSquareSymbol && firstSquareSymbol === secondSquareSymbol && firstSquareSymbol === thirdSquareSymbol) {
        winner = players.find(player => player.playerSymbol === firstSquareSymbol)?.playerName;
      }
    }

    this.winner = winner;
  }

  deriveDraw(): void {
    this.hasDraw = this.gameRounds.length === 9 && !this.winner;
  }

  handleRematch(): void {
    this.gameRounds = [];
    this.gameBoard = [
      [null, null, null],
      [null, null, null],
      [null, null, null]
    ];
    this.winner = undefined;
    this.hasDraw = false;
    this.activePlayer = PlayerSymbol.X;
  }
}
