import {Component, Input} from '@angular/core';
import {GameRound} from "../../app.component";

@Component({
  selector: 'app-log',
  standalone: true,
  imports: [],
  templateUrl: './log.component.html',
  styleUrl: './log.component.css'
})
export class LogComponent {

  @Input() gameRounds: GameRound[] = []

}
