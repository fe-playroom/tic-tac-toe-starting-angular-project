import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-game-over',
  standalone: true,
  imports: [],
  templateUrl: './game-over.component.html',
  styleUrl: './game-over.component.css'
})
export class GameOverComponent {
  @Input() winner: string | undefined = undefined;
  @Input() draw: boolean = false;

  @Output() rematchSelected: EventEmitter<{}>
    = new EventEmitter<{}>();

  onRematch() {
    this.rematchSelected.emit();
  }
}
