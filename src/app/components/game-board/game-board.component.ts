import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PlayerSymbol} from "../../app.component";

@Component({
  selector: 'app-game-board',
  standalone: true,
  imports: [],
  templateUrl: './game-board.component.html',
  styleUrl: './game-board.component.css'
})
export class GameBoardComponent {
  @Input() gameBoard: (PlayerSymbol | null)[][] = [
    [null, null, null],
    [null, null, null],
    [null, null, null]
  ]

  @Output() squareSelected: EventEmitter<{"rowIndex": number, "colIndex": number}>
    = new EventEmitter<{"rowIndex": number, "colIndex": number}>();

  onSquareSelected(rowIndex: number, colIndex: number) {
    this.squareSelected.emit({rowIndex, colIndex});
  }
}
