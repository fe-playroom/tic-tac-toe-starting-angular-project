import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgClass} from "@angular/common";
import {FormsModule} from "@angular/forms";

@Component({
  selector: '[app-player]',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './player.component.html',
  styleUrl: './player.component.css'
})
export class PlayerComponent {
  @Input() active: boolean = false;
  @Input() playerName: string = 'Player'
  @Input() playerSymbol: string = 'X'
  @Output() updateNameEvent: EventEmitter<string> = new EventEmitter<string>();
  editMode: boolean = false;

  handleEdit(): void {
    if (this.editMode) {
      this.updateNameEvent.emit(this.playerName);
    }

    this.editMode = !this.editMode;
  }
}
